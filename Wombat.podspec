Pod::Spec.new do |s|

  s.name         = "Wombat"
  s.version      = "0.2.3.5"
  s.summary      = "Word of Mouth as a Service."

  s.description  = <<-DESC
WORD OF MOUTH FOR MOBILE APPS

It's hard to get in-app messaging right.
Wombat makes it easy.
  DESC
                   
  s.source = {
    :http => 'https://getwombatprod.s3.amazonaws.com/releases/WombatFramework_0.2.3_5.zip'
  }
  s.homepage     = "http://www.getwombat.com"

  s.license      = "Copyright 2016 Wombat Ltd."
  
  s.author             = { "Wombat" => "liron@getwombat.com" }
  s.social_media_url   = "http://twitter.com/getwombat"

  s.platform     = :ios, "8.0"

  s.ios.vendored_frameworks = 'Wombat.framework'

  s.frameworks = "UIKit", "Foundation", "CoreFoundation"
  s.preserve_paths = "strip_architectures_build_phase.sh"

  s.dependency  'AFNetworking'
  s.dependency  'AGWindowView'
  s.dependency  'Bolts'
  s.dependency  'CocoaLumberjack'
  s.dependency  'GVUserDefaults'
  s.dependency  'KeenClient'
  s.dependency  'NSDate-Additions'
  s.dependency  'PureLayout'
  s.dependency  'Reachability'
  s.dependency  'SAMKeychain'
  s.dependency  'UIColor-HexString'
  s.dependency  'UIDevice-Hardware'

end
