//
//  Wombat.h
//  Wombat
//
//  Created by Liron Kopinsky on 01/11/2015.
//  Copyright © 2015 Wombat. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Wombat/WombatTracker.h>
#import <Wombat/WombatNotifications.h>

typedef enum : NSUInteger {
  kWombatLogDebug,
  kWombatLogErrors,
  kWombatLogNone,
} WombatLogLevel;

@interface Wombat : NSObject

/*************
Set up Wombat.
Instructions are available on the Integrate page at https://dash.getwombat.com/integrate
It is best to call this during app launch.


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  [Wombat setupWombat:@"<YOUR_INTEGRATION_CODE"];
  ...

  return YES;
}
*******************/
+(void)setupWombat:(NSString*)appIdentifier;
+(void)setupWombat:(NSString*)appIdentifier
      withLogLevel:(WombatLogLevel)logLevel;

/*******************
Your instance of the Wombat SDK. 
Use this to access the Wombat tracker and to log your users in and out.

 e.g.
[[Wombat sharedInstance] userLoggedIn:@"<UNIQUE_USER_ID>"];
or
[[Wombat sharedInstance].tracker trackMajorEvent:@"PurchaseCompleted"
                                       eventType:@"Purchases"];
*******************/
+(Wombat*)sharedInstance;

/*******************
 Use the WombatTracker to track your in-app events.
 *******************/
@property (nonatomic, strong, readonly) WombatTracker* tracker;

/*******************
 Log in an anonymous user.
 Wombat will automatically generate a unique ID for them.
 *******************/
-(void)anonymousUserLoggedIn;

/*******************
 Log in an anonymous user with a unique ID you created for them.
 *******************/
-(void)anonymousUserLoggedIn:(NSString*)uniqueUserId;

/*******************
 Log in a user with a unique user ID.
 This should be the ID that you use to uniquely identify them in the app.
 *******************/
-(void)userLoggedIn:(NSString*)uniqueUserId;

/*******************
 Logout the user.
 *******************/
-(void)userLoggedOut;

/*******************
 Is the user logged in.
 *******************/
@property (nonatomic, readonly) BOOL isUserLoggedIn;

/*******************
 Disable Wombat
 
 If you want to totally prevent Wombat from showing any UI elements 
 on a particular page set disableWombat = YES. 
 
 IMPORTANT: Make sure to enable it again afterwards.
 *******************/
@property (nonatomic) BOOL disableWombat;

/*******************
 Set Session Timeout

 A new session will start after the app has been inactive for sessionTimeout.
 Default is 20 minutes. Minimum is 30 seconds. Maximum is 6 hours.
 
 NOTE: If you are going to modify this, make sure the sessionTimeout is longer than your app's average session length.
 *******************/
@property (nonatomic) NSTimeInterval sessionTimeout;

/*******************
 Set the logging level.
 *******************/
@property (nonatomic) WombatLogLevel logLevel;

/*******************
 Get the current version of the Wombat SDK.
 *******************/
+(NSString*)wombatVersionNumber;
@end


