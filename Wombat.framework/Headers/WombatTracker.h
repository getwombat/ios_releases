//
//  WombatTracker.h
//  Wombat
//
//  Created by Liron Kopinsky on 02/11/2015.
//  Copyright © 2015 Wombat. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
  kWBMinorEvent,
  kWBMajorEvent
} WombatEventLevel;

@interface WombatTracker : NSObject

@property (nonatomic) NSTimeInterval dispatchFrequencyInSeconds;

-(void)trackPageView:(NSString*)pageName;

-(void)trackMajorEvent:(NSString*)eventName
             eventType:(NSString*)eventType;

-(void)trackMajorEvent:(NSString*)eventName
             eventType:(NSString*)eventType
             extraInfo:(NSDictionary*)extraInfo;

-(void)trackMinorEvent:(NSString*)eventName
             eventType:(NSString*)eventType;

-(void)trackMinorEvent:(NSString*)eventName
             eventType:(NSString*)eventType
             extraInfo:(NSDictionary*)extraInfo;

-(void)trackEvent:(NSString*)eventName
        eventType:(NSString*)eventType
       eventLevel:(WombatEventLevel)level
        extraInfo:(NSDictionary*)extraInfo;
@end
