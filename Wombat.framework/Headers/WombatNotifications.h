//
//  WombatNotifications.h
//  Wombat
//
//  Created by Liron Kopinsky on 10/11/2015.
//  Copyright © 2015 Wombat. All rights reserved.
//

#import <Foundation/Foundation.h>

/*********************
 * A list of the notifications that the WombatSDK makes available.
 *
 * You can listen to these notifications if you need to make
 * UI changes when a Wombat cube will be presented.
 *********************/

//Posted when Wombat setup is completed.
FOUNDATION_EXPORT NSString* const kWombatSetupNotification;

/*********************
 * The rest of the notifications are posted during the display of wombat cubes.
 *
 * Note: A cube might have multiple dialogs in it.
 * For example, a Share Request followed by a Thank You.
 **********************/

//Posted when a Wombat cube starts displaying.
FOUNDATION_EXPORT NSString* const kWombatCubeStarted;

//Posted when a Wombat cube finishes displaying.
FOUNDATION_EXPORT NSString* const kWombatCubeCompleted;


/**********************
 * Notifications during the display of a dialog.
 **********************/

//Posted when a dialog is about to appear.
FOUNDATION_EXPORT NSString* const kWombatDialogWillAppear;
//Posted when a dialog is fully visible.
FOUNDATION_EXPORT NSString* const kWombatDialogDidAppear;
//Posted when a dialog is about to disappear.
FOUNDATION_EXPORT NSString* const kWombatDialogWillDisappear;
//Posted when a dialog has finished disappearing.
FOUNDATION_EXPORT NSString* const kWombatDialogDidDisappear;

@interface WombatNotifications : NSObject

@end
