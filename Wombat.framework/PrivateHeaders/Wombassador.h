//
//  Wombassador.h
//  Wombat
//
//  Created by Liron Kopinsky on 03/11/2015.
//  Copyright © 2015 Wombat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Bolts/Bolts.h>
#import "wbJsonBase.h"

@interface Wombassador : wbJsonBase

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* role;
@property (nonatomic, strong) NSString* imageUrl;

@property (nonatomic, strong) UIImage* image;

-(BFTask*)loadImage;
@end
