//
//  WombatManager+WombatInteral.h
//  Wombat
//
//  Created by Liron Kopinsky on 04/11/2015.
//  Copyright © 2015 Wombat. All rights reserved.
//
#import <Foundation/Foundation.h>

@class WombatDisplayCube;
@class BFTask;

@interface WombatTester : NSObject

+(WombatTester*) tester;

-(BFTask*)showCube:(WombatDisplayCube*)displayCube;

@property (nonatomic) BOOL saveDeviceOnUninstall;
@property (nonatomic) BOOL forceGetCubes;

+(void)useVerboseLogging;

@end
