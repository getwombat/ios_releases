//
//  WombatColorScheme.h
//  Wombat
//
//  Created by Liron Kopinsky on 07/07/2016.
//  Copyright © 2016 Wombat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "wbJsonBase.h"

@interface WombatColorScheme : wbJsonBase

@property (nonatomic, strong) UIColor* heading;
@property (nonatomic, strong) UIColor* headingText;

@property (nonatomic, strong) UIColor* body;
@property (nonatomic, strong) UIColor* titleText;
@property (nonatomic, strong) UIColor* messageText;

@property (nonatomic, strong) UIColor* cta;
@property (nonatomic, strong) UIColor* ctaText;
@property (nonatomic, strong) UIColor* cancelText;

-(instancetype)init;
-(instancetype)initWithJson:(NSDictionary *)dict;
@end
