//
//  JsonBase.h
//  WombatTestApp
//
//  Created by Liron Kopinsky on 07/07/2016.
//  Copyright © 2016 Wombat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface wbJsonBase : NSObject

@property (nonatomic, strong) NSDictionary* json;

-(instancetype)initWithJson:(NSDictionary*)dict;

- (id)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;

//Protected
-(void)extractJsonValues;

@end
