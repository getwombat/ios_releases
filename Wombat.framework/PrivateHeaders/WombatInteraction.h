//
//  WombatInteraction.h
//  Wombat
//
//  Created by Liron Kopinsky on 06/07/2016.
//  Copyright © 2016 Wombat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "wbJsonBase.h"
#import "WombatDisplayCube.h"
#import "InteractionMissingInfo.h"

@class WombatInteractionCompletion;

@interface WombatInteraction : wbJsonBase

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) WombatInteractionCompletion* onSuccess;
@property (nonatomic, strong) WombatInteractionCompletion* onFail;

@property (nonatomic, strong) WombatDisplayCube* parent;

@property (nonatomic, readonly) BOOL ignoreClose;
@property (nonatomic, readonly) BOOL isFirstInteraction;

@property (nonatomic, strong, readonly) NSArray<InteractionMissingInfo*>* missingInfo;

-(BOOL)isMissingInfo;

//protected
-(BFTask*)loadResources;
@end
