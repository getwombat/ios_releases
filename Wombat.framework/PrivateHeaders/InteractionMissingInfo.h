//
//  InteractionMissingInfo.h
//  Wombat
//
//  Created by Liron Kopinsky on 15/12/2016.
//  Copyright © 2016 Wombat. All rights reserved.
//

#import "wbJsonBase.h"

@interface InteractionMissingInfo : wbJsonBase

@property (nonatomic, strong) NSString* key;
@property (nonatomic, strong) NSArray<NSString*>* platforms;
@property (nonatomic, strong) NSString* desc;

-(BOOL)isMissingForiOS;

@end
