//
//  WombatCube.h
//  WombatTestApp
//
//  Created by Liron Kopinsky on 06/07/2016.
//  Copyright © 2016 Wombat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "wbJsonBase.h"
#import "WombatColorScheme.h"
#import "Wombassador.h"

@class WombatInteraction;

@interface WombatDisplayCube : wbJsonBase

@property (nonatomic, strong) NSString* cubeId;
@property (nonatomic, strong) NSString* cubeType;

@property (nonatomic, strong) Wombassador* wombassador;
@property (nonatomic, strong) WombatColorScheme* colorScheme;

@property (nonatomic, strong) NSDictionary* placeholders;

@property (nonatomic, strong) NSArray<WombatInteraction*>* interactions;

-(BFTask*)loadResources;

-(BOOL)isMissingInfo;
-(WombatInteraction*)firstMissingInfo;

@end
